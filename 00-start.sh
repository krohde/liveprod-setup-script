#!/bin/bash

### FUNCTIONS ###
exec_ansible () {
    curl -s https://gitlab.com/krohde/liveprod-setup-script/raw/master/$1 > $1
    ansible-playbook $1
}

#Install Ansible
curl -s https://gitlab.com/krohde/liveprod-setup-script/raw/master/01-ansible-install.sh > 01-ansible-install.sh
chmod +x 01-ansible-install.sh
./01-ansible-install.sh

#Execute Ansible script to configure local machine
exec_ansible 02-ansible-script.yaml 

#Execute Ansible script to install and configure postgresql
exec_ansible 10-postgre-install-ansible.yaml

#Execute Ansible script to install google Chrome
exec_ansible 11-google-chrome.yaml


exit

#Configure local sources
cp /etc/apt/sources.list.d/official-package-repositories.list /etc/apt/sources.list.d/official-package-repositories.list.bak
sed -i 's|http://packages.linuxmint.com|http://mirrors.evowise.com/linuxmint/packages|g' /etc/apt/sources.list.d/official-package-repositories.list
sed -i 's|http://archive.ubuntu.com|http://ftp.icm.edu.pl/pub/Linux|g' /etc/apt/sources.list.d/official-package-repositories.list

# Install VM tools first 
apt update && apt install open-vm-tools-desktop -y

# Upgrade distro to newest version
apt update && apt full-upgrade -y

# Install management software
apt update && apt install ansible -y

# Install Development tools

#GUI CONFIGURATION SETTINGS
# Configure terminal
dconf write /org/mate/terminal/profiles/default/background-color \'#000000000000\'
dconf write /org/mate/terminal/profiles/default/bold-color \'#000000000000\'
dconf write /org/mate/terminal/profiles/default/cursor-blink-mode \'on\'
dconf write /org/mate/terminal/profiles/default/cursor-shape \'block\'
dconf write /org/mate/terminal/profiles/default/foreground-color \'#0000FFFF0000\'
dconf write /org/mate/terminal/profiles/default/scrollback-unlimited true
dconf write /org/mate/terminal/profiles/default/use-theme-colors false
dconf write /org/mate/terminal/profiles/default/visible-name \'Z\'

# Configure the text editor xed
dconf write /org/x/editor/preferences/editor/display-line-numbers true
dconf write /org/x/editor/preferences/editor/insert-spaces false

# Configure the file explorer Caja
dconf write /org/mate/caja/preferences/default-folder-viewer \'list-view\'
dconf write /org/mate/caja/preferences/show-hidden-files true
dconf write /org/mate/caja/preferences/executable-text-activation \'display\'
dconf write /org/mate/caja/preferences/date-format \'iso\'
dconf write /org/mate/caja/preferences/show-image-thumbnails \'never\'
dconf write /org/mate/caja/preferences/show-directory-item-counts \'always\'
dconf write /org/mate/caja/list-view/default-column-order "['size', 'name', 'type', 'date_modified', 'date_accessed', 'group', 'where', 'mime_type', 'octal_permissions', 'owner', 'permissions', 'selinux_context']"
dconf write /org/mate/caja/list-view/default-visible-columns "['size', 'name', 'type', 'date_modified']"
dconf write /org/mate/caja/window-state/side-pane-view \'tree\'

# Configure the Desktop
dconf write /org/mate/caja/desktop/computer-icon-visible false
dconf write /org/mate/caja/desktop/home-icon-visible false
dconf write /org/mate/caja/desktop/network-icon-visible false
dconf write /org/mate/caja/desktop/volumes-visible false

# Configure Window
dconf write /org/mate/Marco/general/focus-mode \'sloppy\'

# Configure the Panel
dconf write /org/mate/panel/toplevels/bottom/size 40

# Configure screensaver / Power Management
dconf write /org/mate/session/idle-delay 60
dconf write /org/mate/power-manager/sleep-display-ac 3600

#Configure the keyboard; xmodmap will replace the nobreakspace character to a normal regular space, nobreakspace is an hidden character which can cause problems when you code.
xmodmap -pke > ~/.Xmodmap; cat ~/.Xmodmap | sed 's/nobreakspace/space/g' > ~/.Xmodmap
gsettings set org.mate.peripherals-keyboard delay 280
gsettings set org.mate.peripherals-keyboard rate 70
gsettings set org.mate.interface cursor-blink-time 1200

#Configure the mouse, faster and make it so that the window raise when you hover it
gsettings set org.mate.peripherals-mouse motion-acceleration 10.0
gsettings set org.mate.peripherals-mouse motion-threshold 10
gsettings set org.mate.Marco.general focus-mode 'sloppy'


#MANUAL STEPS
#1. Configure Timeshift
